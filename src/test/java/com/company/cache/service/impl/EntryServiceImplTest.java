package com.company.cache.service.impl;

import com.company.cache.config.TestConfig;
import com.company.cache.configuration.CacheConfig;
import com.company.cache.entity.Entry;
import com.company.cache.exception.EntryNotFoundException;
import com.company.cache.repository.EntryRepository;
import com.company.cache.service.EntryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfig.class, CacheConfig.class})
public class EntryServiceImplTest {

    @Autowired
    private EntryService entryService;
    @Autowired
    private EntryRepository entryRepository;

    @Test
    public void testCacheWithSizeRestriction() {
        try {
            entryService.getEntry("999");
            assert false;
        } catch (EntryNotFoundException ignore) {
            // it's ok
        }
        verify(entryRepository, times(1)).findById("999");
        fillCache();
        Entry savedEntry = entryService.getEntry("999");
        verify(entryRepository, times(1)).findById("999");
        assertNotNull(savedEntry);
        assertEquals(savedEntry.getValue(), "122");
        savedEntry.setValue("133");
        entryService.putEntry(savedEntry);
        Entry newSavedEntry = entryService.getEntry("999");
        verify(entryRepository, times(1)).findById("999");
        assertNotNull(newSavedEntry);
        assertEquals(savedEntry.getValue(), "133");

    }


    private void fillCache() {
        Entry entry;
        for(int i = 0; i<1000; i++ ) {
            entry = new Entry();
            entry.setKey(String.valueOf(i));
            entry.setValue("122");
            when(entryRepository.save(entry)).thenReturn(entry);
            entryService.putEntry(entry);
        }
    }

}