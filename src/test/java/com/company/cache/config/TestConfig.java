package com.company.cache.config;

import com.company.cache.repository.EntryRepository;
import com.company.cache.service.EntryService;
import com.company.cache.service.impl.EntryServiceImpl;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public EntryService entryService() {
        return new EntryServiceImpl();
    }

    @Bean
    public EntryRepository mockRepository() {
        return Mockito.mock(EntryRepository.class);
    }



}
