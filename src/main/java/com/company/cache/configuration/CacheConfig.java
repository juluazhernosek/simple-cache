package com.company.cache.configuration;

import com.company.cache.service.EntryService;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableCaching
@EnableScheduling
public class CacheConfig {

    @Value("${cache.memoryStoreEvictionPolicy}")
    private String memoryStoreEvictionPolicy;
    @Value("${cache.maxBytesLocalHeapPercent}")
    private String maxBytesLocalHeap;
    @Value("${cache.timeToLiveSeconds}")
    private Long timeToLiveSeconds;

    @Autowired
    private EntryService entryService;

    public net.sf.ehcache.CacheManager ehCacheManager() {
        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setName("entry-cache");
        cacheConfiguration.setMemoryStoreEvictionPolicy(memoryStoreEvictionPolicy);
        cacheConfiguration.setTimeToLiveSeconds(timeToLiveSeconds);
        cacheConfiguration.setMaxBytesLocalHeap(maxBytesLocalHeap);

        net.sf.ehcache.config.Configuration config = new net.sf.ehcache.config.Configuration();
        config.setMaxBytesLocalHeap(maxBytesLocalHeap);
        config.addCache(cacheConfiguration);

        return net.sf.ehcache.CacheManager.newInstance(config);
    }

    @Bean
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheManager());
    }

    @Scheduled(fixedRateString = "${cache.schedule.delayInMilliSeconds}")
    public void removeOldRecords() {
        entryService.removeByTtl(timeToLiveSeconds);
    }


}
