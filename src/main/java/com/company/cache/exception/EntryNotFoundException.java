package com.company.cache.exception;

public class EntryNotFoundException extends RuntimeException {

    private final String key;

    public EntryNotFoundException(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
