package com.company.cache.service;

import com.company.cache.entity.Entry;

public interface EntryService {

    /**
     * Get entry from cache by key
     *
     * @param key
     * @return entry from cache
     */
    Entry getEntry(String key);

    /**
     * Put entry to cache
     *
     * @param entry
     */
    Entry putEntry(Entry entry);

    /**
     * Remove expired entries from cache
     *
     * @param ttlInSeconds - ttl
     */
    void removeByTtl(long ttlInSeconds);

}
