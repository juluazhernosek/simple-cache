package com.company.cache.service.impl;

import com.company.cache.entity.Entry;
import com.company.cache.exception.EntryNotFoundException;
import com.company.cache.repository.EntryRepository;
import com.company.cache.service.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service
@CacheConfig(cacheNames = {"entry-cache"})
public class EntryServiceImpl implements EntryService {

    @Autowired
    private EntryRepository entryRepository;


    @Cacheable
    public Entry getEntry(String key) {
        return entryRepository.findById(key).orElseThrow(() -> new EntryNotFoundException(key));
    }

    @Transactional
    @CachePut(key = "#entry.key")
    public Entry putEntry(Entry entry) {
        return entryRepository.save(entry);
    }

    @Override
    @Transactional
    public void removeByTtl(long ttlInSeconds) {
        entryRepository.removeByTime(LocalDateTime.now().minus(ttlInSeconds, ChronoUnit.SECONDS));
    }

}
