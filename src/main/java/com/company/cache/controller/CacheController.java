package com.company.cache.controller;

import com.company.cache.entity.Entry;
import com.company.cache.exception.EntryNotFoundException;
import com.company.cache.service.EntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class CacheController {

    private final static Logger LOGGER = LoggerFactory.getLogger(CacheController.class);

    @Autowired
    private EntryService entryService;

    @GetMapping("/{key}")
    public Entry get(@PathVariable String key) {
        return entryService.getEntry(key);
    }

    @PostMapping
    public void put(@RequestBody Entry entry) {
        entryService.putEntry(entry);
    }

    @ExceptionHandler(EntryNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private void handleEntryNotFound(EntryNotFoundException e) {
        LOGGER.error("Entry with key {} was not found", e.getKey());
    }
}
