package com.company.cache.repository;

import com.company.cache.entity.Entry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;

public interface EntryRepository extends JpaRepository<Entry, String> {

    @Modifying(flushAutomatically = true)
    @Query("delete from Entry e where e.timestamp < ?1")
    void removeByTime(LocalDateTime time);
}
